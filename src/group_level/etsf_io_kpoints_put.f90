!{\src2tex{textfont=tt}}
!!****m* etsf_kpoints/etsf_io_kpoints_put
!! NAME
!!  etsf_io_kpoints_put
!!
!! FUNCTION
!!    Write data related to the given group in an opened ETSF file (it must be in
!!    write mode, use etsf_io_low_set_write_mode() to change it).
!!    Only associated pointers of argument @folder will be accessed. If any errors
!!    occurs it returns with @lstat = .false..
!!
!! COPYRIGHT
!!  Copyright (C) 2006-2010 (Damien Caliste)
!!  This file is distributed under the terms of the
!!  GNU Lesser General Public License, see the COPYING file
!!  or http://www.gnu.org/copyleft/lesser.txt .
!!
!! INPUTS
!! * ncid = 
!!     integer returned by an 'open' NetCDF call. The file can be 
!!     either in define or write mode. This status can be changed
!!     by the call.
!! * folder <type(etsf_kpoints)> = 
!!     an allocated structure with pointers on allocated areas in memory.
!!     These areas will be read or written if the pointer is associated, if
!!     not, the variable will be ignored. It is possible to access to specific
!!     dimensions of a variable using the <short_var_name>__kpoint_access or
!!     <short_var_name>__spin_access of this @folder structure. The 
!!     <short_var_name>__number_of_<something> can also been set if only a subpart
!!     in one dimension should be accessed (this is possible when the specifications
!!     have been declared with a max_number_of_<something>.
!! OUTPUT
!! * lstat = 
!!     return .true. if all the actions succeed, if not the status
!!     of the file is undefined.
!! * error_data <type(etsf_io_low_error)> = 
!!     contains the details of the error is @lstat is false.
!!
!! NOTES
!!  This file has been automatically generated by the autogen_subroutines.py
!!  script. Any change you would bring to it will systematically be
!!  overwritten.
!!
!! SOURCE
subroutine etsf_io_kpoints_put(ncid, folder, lstat, error_data)

  !Arguments ------------------------------------
  integer, intent(in) :: ncid
  type(etsf_kpoints), intent(in) :: folder
  logical, intent(out) :: lstat
  type(etsf_io_low_error), intent(out) :: error_data

  !Local variables-------------------------------
  character(len = *), parameter :: my_name = 'etsf_io_kpoints_put'
  integer,allocatable :: varid(:)
  integer,allocatable :: start(:)
  integer,allocatable :: count(:)
  integer :: len
  character(etsf_charlen) :: flag


  ! *************************************************************************

!DEBUG
!write (*,*) 'etsf_io_kpoints_put : enter'
!ENDDEBUG

  
  allocate(varid(5))
  ! Begin by putting the file in write mode.
  call etsf_io_low_set_write_mode(ncid, lstat, error_data = error_data)
  if (.not. lstat) then
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  
  if (associated(folder%kpoint_grid_shift)) then
    call etsf_io_low_write_var(ncid, "kpoint_grid_shift", &
                            & folder%kpoint_grid_shift, &
                            & lstat, ncvarid = varid(1), &
                            & error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
  end if
  
  if (associated(folder%kpoint_grid_vectors)) then
    call etsf_io_low_write_var(ncid, "kpoint_grid_vectors", &
                            & folder%kpoint_grid_vectors, &
                            & lstat, ncvarid = varid(2), &
                            & error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
  end if
  
  if (associated(folder%monkhorst_pack_folding)) then
    call etsf_io_low_write_var(ncid, "monkhorst_pack_folding", &
                            & folder%monkhorst_pack_folding, &
                            & lstat, ncvarid = varid(3), &
                            & error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
  end if
  
  if (associated(folder%reduced_coordinates_of_kpoints)) then
    call etsf_io_low_write_var(ncid, "reduced_coordinates_of_kpoints", &
                            & folder%reduced_coordinates_of_kpoints, &
                            & lstat, ncvarid = varid(4), &
                            & error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
  end if
  
  if (associated(folder%kpoint_weights)) then
    call etsf_io_low_write_var(ncid, "kpoint_weights", &
                            & folder%kpoint_weights, &
                            & lstat, ncvarid = varid(5), &
                            & error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
  end if
  
  deallocate(varid)

!DEBUG
!write (*,*) 'etsf_io_kpoints_put : exit'
!ENDDEBUG

end subroutine etsf_io_kpoints_put
!!***
